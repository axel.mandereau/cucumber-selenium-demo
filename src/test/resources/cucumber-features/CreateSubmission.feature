Feature: Create Submission

  Scenario: User can create a submission
    Given a logged in user on home
    When the user creates a submission
    Then the user return to home