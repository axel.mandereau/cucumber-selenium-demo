package io.github.mayurhalai.e2e.pages;

import io.github.mayurhalai.annotation.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import static io.github.mayurhalai.global.TestConstants.WAIT_TIME;

@PageObject
public class HomePage {
    @Autowired
    private WebDriver webDriver;

    @FindBy(className = "page-title")
    private WebElement homePageTitle;

    @FindBy(css = "body > app-root > div > div.globals > oir-navigation > div > ul:nth-child(3) > li:nth-child(3) > div")
    private WebElement createSubmissionNavButton;

    @FindBy(className = "overlay")
    private WebElement overLay;

    public boolean isDisplayed() {
        WebDriverWait wait = new WebDriverWait(webDriver, WAIT_TIME);
        return wait.until(ExpectedConditions.visibilityOf(homePageTitle)).isDisplayed();
    }

    public void createSubmission() {
        WebDriverWait wait = new WebDriverWait(webDriver, WAIT_TIME);
        wait.until(ExpectedConditions.invisibilityOf(overLay));
        wait.until(ExpectedConditions.elementToBeClickable(createSubmissionNavButton));
        createSubmissionNavButton.click();
    }
}
