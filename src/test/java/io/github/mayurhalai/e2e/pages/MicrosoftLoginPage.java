package io.github.mayurhalai.e2e.pages;

import io.github.mayurhalai.annotation.PageObject;
import io.github.mayurhalai.e2e.model.UserCredentials;
import io.github.mayurhalai.utils.WaitUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import static io.github.mayurhalai.global.TestConstants.WAIT_TIME;

@PageObject
public class MicrosoftLoginPage {
    @Autowired
    private WebDriver webDriver;

    @FindBy(name = "loginfmt")
    private WebElement emailInput;

    @FindBy(id = "idSIButton9")
    private WebElement emailNextButton;

    @FindBy(name = "passwd")
    private WebElement passwordInput;

    @FindBy(id = "idBtn_Back")
    private WebElement rememberSignInNoButton;

    @FindBy(id = "idSIButton9")
    private WebElement signInButton;

    public void login(UserCredentials credentials) {
//        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.jsReturnsValue("document.ready()"));
        WaitUtil.staticWait(1);
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(emailInput));
        emailInput.clear();
        emailInput.sendKeys(credentials.getEmail());
        emailNextButton.click();
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(passwordInput));
        passwordInput.clear();
        passwordInput.sendKeys(credentials.getPassword());
        signInButton.click();
        rememberSignInNoButton.click();
    }
}
