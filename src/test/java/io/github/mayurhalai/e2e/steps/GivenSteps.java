package io.github.mayurhalai.e2e.steps;

import cucumber.api.java.en.Given;
import io.github.mayurhalai.config.TestConfiguration;
import io.github.mayurhalai.e2e.model.UserCredentials;
import io.github.mayurhalai.e2e.pages.MicrosoftLoginPage;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfiguration.class)
@Scope("test")
public class GivenSteps {
    @Autowired
    private WebDriver driver;
    @Autowired
    private MicrosoftLoginPage loginPage;
    @Autowired
    private Environment environment;
    @Autowired
    private UserCredentials credentials;

    @Given("the user with email {string} and password {string}")
    public void theUserWithEmailAndPassword(String email, String password) {
        credentials.setEmail(email);
        credentials.setPassword(password);
    }

    @Given("a logged in user on home")
    public void aLoggedInUserOnHome() {
        credentials.setEmail(environment.getProperty("email"));
        credentials.setPassword(environment.getProperty("password"));
        driver.get("https://submissionssystemtest.azurewebsites.net ");
        loginPage.login(credentials);
    }
}
