package io.github.mayurhalai.e2e.steps;

import cucumber.api.java.en.Then;
import io.github.mayurhalai.e2e.pages.HomePage;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

@Scope("test")
public class ThenSteps {
    @Autowired
    private HomePage homePage;

    @Then("the user see dashboard")
    public void theUserSeeDashboard() {
        Assert.assertTrue(homePage.isDisplayed());
    }

    @Then("the user return to home")
    public void theUserReturnToHome() {
        Assert.assertTrue(homePage.isDisplayed());
    }
}
