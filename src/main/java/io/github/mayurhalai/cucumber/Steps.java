package io.github.mayurhalai.cucumber;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.mayurhalai.spring.TestScope;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.TimeUnit;

@Scope("test")
public class Steps {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private WebDriver webDriver;

    @Before
    public void beforeScenario(Scenario scenario) {
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
//        webDriver.manage().window().setSize(new Dimension(1280, 780));
    }

    @After
    public void afterScenario(Scenario scenario) {
        scenario.write("Attaching screenshot");
        scenario.embed(getScreenShot(), "image/png");
        webDriver.close();
        context.getBean(TestScope.class).reset();
    }

    private byte[] getScreenShot() {
        return ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
    }
}
